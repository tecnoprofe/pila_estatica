
using System;
namespace PilaED
{
    class Program
    {
        static void Main(string[] args)
        {
            Pila p = new Pila(100);
            p.insertar("hola");
            p.insertar("mundo");
            p.insertar("loco por");            
            p.insertar("la programacion");
            p.quitar();
            p.insertar("El Majadito");

            if(p.aux){
                Console.WriteLine("Pila llena");
            }
            else{
                while (p.tope != 0)
                {
                    Console.WriteLine(p.quitar());
                }
            }
            
            Console.ReadKey();
        }
    }
    class Pila {
        public int tope;
        public string[] informacion;
        public int tamaño;

        public bool aux;


        public Pila(int tama) {
            this.tope = 0;
            this.tamaño = tama;
            this.informacion = new string[tamaño];
            aux=false;
        }
        public void insertar(string cadena){
            if(tope!=tamaño){
                this.informacion[tope] = cadena;
                this.tope++;                
            }
            else{
                aux=true;
                Console.WriteLine("Desborde");
            }            
        }
        public string quitar()
        {
            tope--;
            return informacion[tope];
        }
    }

}
